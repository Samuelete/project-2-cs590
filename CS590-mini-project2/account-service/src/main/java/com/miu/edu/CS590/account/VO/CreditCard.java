package com.miu.edu.CS590.account.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCard {
    private  Integer creditCardId;
    private  String creditCardName;
    private  String cardNumber;
    private Integer ccv;
    private LocalDate expiryDate;
}
