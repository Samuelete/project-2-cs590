package com.miu.edu.CS590.account.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miu.edu.CS590.account.VO.ResponseEntityVo;
import com.miu.edu.CS590.account.model.UserAccount;
import com.miu.edu.CS590.account.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class AccountController {
    @Autowired
    AccountService accountService;

    @PostMapping("/account")
    public UserAccount saveAccount(@RequestBody UserAccount account){
        log.info("Inside Account controller ");
        return accountService.saveAccount(account);
    }

    @GetMapping("/account")
    public List<UserAccount> getAccounts(){
        log.info("Inside Account controller Get all user accounts ");
        return accountService.getAccount();
    }

    @PutMapping("/account")
    public ResponseEntity<?> updateInternalAccount(@RequestBody String userAccount) {
        System.out.println("INSIDE OF THE SHIPPING CONTROLLER");
        ObjectMapper objectMapper = new ObjectMapper();
        UserAccount tempOrder = null;
        try {
            tempOrder = objectMapper.readValue(userAccount, UserAccount.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        accountService.saveAccount(tempOrder);
        return new ResponseEntity<>("The Account has been"
                + " Updated", HttpStatus.OK);
    }

    @GetMapping("/account-internal/{accountId}")
    public UserAccount getAccount(@PathVariable Integer accountId) {
        return accountService.getAccountById(accountId);
    }

    @GetMapping("/account/{accountId}")
    public ResponseEntityVo getAccountById(@PathVariable Integer accountId){
        log.info("Inside Account controller ");
        return accountService.getAccountByTheId(accountId);
    }
}
