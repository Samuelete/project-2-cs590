package com.miu.edu.CS590.account.service;

import com.miu.edu.CS590.account.VO.BankAccount;
import com.miu.edu.CS590.account.VO.CreditCard;
import com.miu.edu.CS590.account.VO.PayPal;
import com.miu.edu.CS590.account.VO.ResponseEntityVo;
import com.miu.edu.CS590.account.model.UserAccount;
import com.miu.edu.CS590.account.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
public class AccountService {
    @Autowired
    private AccountRepository  accountRepository;

    @Autowired
    private RestTemplate restTemplate;


    public UserAccount saveAccount(UserAccount account) {
        log.info("Inside Account controller ");
        return  accountRepository.save(account);
    }

    public List<UserAccount> getAccount(){
        return  accountRepository.findAll();
    }

    public UserAccount getAccountById(Integer accountId){
       return accountRepository.findById(accountId).get();
    }

    public ResponseEntityVo getAccountByTheId(Integer accountId) {
        UserAccount user = accountRepository.findById(accountId).get();
        CreditCard creditCard = restTemplate.getForObject("http://CREDIT-CARD-SERVICE/credit-card/{creditCardId}", CreditCard.class, user.getCreditCardId());
        BankAccount bankAccount = restTemplate.getForObject("http://BANK-ACCOUNT-SERVICE/bank-id/{bankId}", BankAccount.class, user.getBankAccountId());
        PayPal paypal = restTemplate.getForObject("http://PAY-PAL-SERVICE/pay-pal/{payPalId}", PayPal.class, user.getPayPalId());
        ResponseEntityVo responseEntityVo = new ResponseEntityVo(user,creditCard, paypal, bankAccount);
        return responseEntityVo;
    }
}
