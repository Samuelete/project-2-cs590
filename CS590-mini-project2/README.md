## Project 2 ##

## Authors: ##

    Samuel Bartolome Valiente
    Godwin Tusime

# Requirements #

 - Docker
 - Kubernetes
 - Minikube
 - Optional: kubernetes-port-forwarder app. LINK: https://kube-forwarder.pixelpoint.io/
   - Install this program to open port very easily to give external access to your application in the cluster
   - If you do not want to user this application, then kubectl port-forwarder command is needed.
 - MySQL: our Root password is 1234. Maybe if this password does not match with yours, mysql can create conflict and refuse your connection with the 
application.

# Steps #

    1. Extract the zip file wherever you want
    2. It contains the source code of the whole project. Open the folder called "kubernetes-files". Our .yml files are here.
    3. Open your terminal and start you cluster (minikube)
    4. Once is open go to the services directory on your terminal and execute this command: kubectl apply -f ./
    5. Make sure that all services are running and the containers are created. Do not start the production files until this is running.
    6. Go to the production directory and execute this files with the same command given above.
    7. If everthing went fine, all services should be running and working

# Opening ports #

 - If you do not want to user the kubernetes-port-forwarder application then you need to open the gateway port and the eureka port in case
 you want to see which services are communicating with Eureka.
 - Eureka: kubectl port-forward pod/eureka-0 8761:8761
 - Gateway: kubectl port-forward <gateway pod name> 9191:9191   (Any of the two ones that are running...)
 - Now you can go to postman and check the Endpoints.

# Postman steps #

 - Open this ends points in order to create the necessary data to make the order:
 - Execute these endpoints just once, especially the product one
 
    1. URL= localhost:9191/products Method= POST
       - JSON Object:
       {
       "name": "Play 5",
       "vendor": "Best Buy",
       "category": "VideoGames",
       "quantity": 20
       }
    2. URL= localhost:9191/account Method= POST
       - JSON Object
         {
         "firstName": "Moses Godwin",
         "lastName": "Isreal",
         "email":"isreal@gmail.com",
         "shippingAddress":"1000 N 4th St Fairfeild, IA",
         "creditCardId": 1,
         "payPalId":1,
         "bankAccountId": 1,
         "defaultPaymentMethod": null
         }
    3. URL= localhost:9191/credit-card/ , Method= POST
        - JSON Object
          {
          "creditCardName":"Discover",
          "cardNumber": "1234 567 9089",
          "ccv": 123,
          "expiryDate": "2024-12-12"
          }
    4. URL= localhost:9191/pay-pal/ , Method= POST
       - JSON Object
         {
         "userName": "samuel@gmail.com",
         "payPalDetails":"Thank you for paying with pay pal"
         }
    5. URL= localhost:9191/bank , Method= POST
       - JSON Object
         {
         "name":"Checking",
         "routing": "698798 8098",
         "number": "67644 9893"
         }
    Last Step: Make the order:
    6. URL= localhost:9191/order/place-order , Method: POST
    - JSON Object
      {
      "shippingInfo": "1000N 4ST MIU",
      "paymentMethod":"PayPal",
      "userAccountId":1,
      "products":
      [{ "name":"Play 5", "quantity": 6}]
      }

 - If everything went fine you should get an output like this:

[
{
"headers": {},
"body": "[{\"headers\":{},\"body\":\"The Payment went through successfully\",\"statusCodeValue\":200,\"statusCode\":\"OK\"},{\"headers\":{},\"body\":\"Mr/Miss Moses Godwin The PayPal Transaction have been made successfully\",\"statusCodeValue\":200,\"statusCode\":\"OK\"}]",
"statusCode": "OK",
"statusCodeValue": 200
},
{
"headers": {},
"body": "The Order has been shipped to 1000N 4ST MIU it will arrive soon",
"statusCode": "OK",
"statusCodeValue": 200
},
{
"headers": {},
"body": "The Order has been made successfully",
"statusCode": "OK",
"statusCodeValue": 200
}
]

### Final Important Thoughts ###

After many approaches and trial and error, we have not been able to implement the security part. We were able to implement Keycloak in our
project locally, and we could even make it run inside the cluster. However, we could not make it communicate without application.

Same situation we had with Zipkin.

We are so sorry for this, but we have done our best without success.

Please if you have questions let us know on teams. If an image does not pull, please let me know as soon as possible to upload it again.

Thank you!!!!!