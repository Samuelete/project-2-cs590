package com.miu.edu.CS590.order.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miu.edu.CS590.order.VO.*;
import com.miu.edu.CS590.order.model.Order;
import com.miu.edu.CS590.order.model.OrderDTO;
import com.miu.edu.CS590.order.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    RestTemplate restTemplate;

    public List <ResponseEntity<String>> placeOrder(OrderDTO orderDTO) throws JsonProcessingException {

        // SAM'S CODE PART
        List <Product> products = orderDTO.getProducts();
        List <ResponseEntity<String>> httpResponses = new ArrayList<>();
        log.info("INSIDE OF CHECK METHOD");
        for(Product product : products) {
            System.out.println(product);
            Product tempProduct = restTemplate.getForObject("http://PRODUCT-SERVICE/products/{productName}", Product.class, product.getName());
            if(tempProduct.getQuantity() <= product.getQuantity()) {
                httpResponses.add(new ResponseEntity<String>("The product " + tempProduct.getName() + " does not have enough stock " +
                        " Please modify the quantity and try again", HttpStatus.OK));
                return httpResponses;
            } else {
                restTemplate.put("http://PRODUCT-SERVICE/products/{productName}/{quantity}", Product.class, product.getName(), product.getQuantity());
            }
        }

        log.info("OUTSIDE OF THE LOOP AND END OF SAM'S CODE");

        Order tempOrder = convertOrderDTO(orderDTO);
        Order order1 = orderRepository.saveAndFlush(tempOrder);

        // GODWIN'S CODE PART
        UserAccount account = restTemplate.getForObject("http://ACCOUNT-SERVICE/account-internal/{accountId}",UserAccount.class,order1.getUserAccountId());
        assert account != null;

        log.info("Info Account : "+account.toString());
        log.info("BEFORE ACCOUNT UPDATE");
        // SET ACCOUNT AND UPDATE
        account.setDefaultPaymentMethod(order1.getPaymentMethod());
        ObjectMapper objectMapper = new ObjectMapper();
        String orderString = objectMapper.writeValueAsString(account);
        restTemplate.put("http://ACCOUNT-SERVICE/account", orderString, String.class);

        log.info("BEFORE PAYMENT PART ORDER " + account.getFirstName());
        // PAYMENT METHOD PART
        PaymentMethod paymentMethod = new PaymentMethod(order1.getPaymentMethod(), account.getFirstName());
        String paymentString = objectMapper.writeValueAsString(paymentMethod);
        log.info("Payment Info: " + paymentMethod.getPaymentInfo(), account.getFirstName());

        httpResponses.add(new ResponseEntity<String>(restTemplate.postForObject("http://PAYMENT-METHOD-SERVICE/payment", paymentString, String.class), HttpStatus.OK));

        // SHIPPING PART
        String orderShippingString = objectMapper.writeValueAsString(order1);
        httpResponses.add(new ResponseEntity<String>(restTemplate.postForObject("http://SHIPPING-SERVICE/shipping", orderShippingString, String.class), HttpStatus.OK));
        log.info("Shipping Order...");

        httpResponses.add(new ResponseEntity<String>("The Order has been made successfully", HttpStatus.OK));

        return httpResponses;
    }

    private Order convertOrderDTO(OrderDTO orderDTO) {

        StringBuffer sb = new StringBuffer();
        Order tempOrder = new Order();
        tempOrder.setShippingInfo(orderDTO.getShippingInfo());
        tempOrder.setPaymentMethod(orderDTO.getPaymentMethod());
        tempOrder.setUserAccountId(orderDTO.getUserAccountId());
        for(Product product : orderDTO.getProducts()) {
            sb.append(product.getName() + "= ").append(product.getQuantity()+",");
        }
        tempOrder.setProducts(sb.toString());
        return tempOrder;
    }

    public List<Order> findOrders(){
        return orderRepository.findAll();
    }

}
