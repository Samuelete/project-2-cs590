package com.miu.edu.CS590.order.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.miu.edu.CS590.order.model.Order;
import com.miu.edu.CS590.order.model.OrderDTO;
import com.miu.edu.CS590.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/")
    public List<Order> getOrders(){
       return orderService.findOrders();
    }

    @PostMapping("/test")
    public Order testing(@RequestBody Order order) {
        Order tempOrder = order;
        System.out.println(order);
        return tempOrder;
    }

    @PostMapping("/place-order")
    public List<ResponseEntity<String>> placeOrder(@RequestBody OrderDTO orderDTO) throws JsonProcessingException {
        List <ResponseEntity<String>> orderSuccess = orderService.placeOrder(orderDTO);
        return orderSuccess;
    }

}
