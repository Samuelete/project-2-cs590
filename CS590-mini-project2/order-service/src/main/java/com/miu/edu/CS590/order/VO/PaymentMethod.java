package com.miu.edu.CS590.order.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PaymentMethod {
    private String PaymentInfo;
    private String customerName;
}
