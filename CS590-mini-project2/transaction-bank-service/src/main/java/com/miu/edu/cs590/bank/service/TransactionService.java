package com.miu.edu.cs590.bank.service;

import com.miu.edu.cs590.bank.model.PaymentMethod;
import org.springframework.http.ResponseEntity;

public interface TransactionService {

    ResponseEntity<String> transactionReceived(PaymentMethod paymentTemp);
}
