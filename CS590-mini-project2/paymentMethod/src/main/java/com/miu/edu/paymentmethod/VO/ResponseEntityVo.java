package com.miu.edu.paymentmethod.VO;


import com.miu.edu.paymentmethod.model.PaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseEntityVo {
    private PaymentMethod paymentMethod;
    public CreditCard creditCard;

}
