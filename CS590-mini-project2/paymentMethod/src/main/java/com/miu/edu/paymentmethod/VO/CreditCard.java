package com.miu.edu.paymentmethod.VO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreditCard {
    private  Integer creditCardId;
    private  String creditCardName;
}
