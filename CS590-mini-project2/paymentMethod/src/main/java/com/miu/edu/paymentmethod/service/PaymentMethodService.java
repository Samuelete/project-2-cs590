package com.miu.edu.paymentmethod.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miu.edu.paymentmethod.model.PaymentMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class PaymentMethodService {


    @Autowired
    private RestTemplate restTemplate;

    public ResponseEntity<String> sendTransaction(PaymentMethod paymentInfo) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        String paymentString = objectMapper.writeValueAsString(paymentInfo);
        ResponseEntity<String> httpResponse;

        switch (paymentInfo.getPaymentInfo()) {
            case "CreditCard": httpResponse = new ResponseEntity<>(restTemplate.postForObject("http://TRANSACTION-CREDIT-CARD-SERVICE/transaction", paymentString, String.class), HttpStatus.OK);
                break;
            case "PayPal": httpResponse = new ResponseEntity<>(restTemplate.postForObject("http://TRANSACTION-PAYPAL-SERVICE/transaction", paymentString, String.class), HttpStatus.OK);
                break;
            case "BankAccount": httpResponse = new ResponseEntity<>(restTemplate.postForObject("http://TRANSACTION-BANK-SERVICE/transaction", paymentString, String.class), HttpStatus.OK);
                break;
            default:  httpResponse = new ResponseEntity<>("Transaction Failed", HttpStatus.BAD_REQUEST);
        }

        return httpResponse;
    }
}
