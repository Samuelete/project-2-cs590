package com.miu.edu.paymentmethod.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miu.edu.paymentmethod.VO.CreditCard;
import com.miu.edu.paymentmethod.model.PaymentMethod;
import com.miu.edu.paymentmethod.service.PaymentMethodService;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import java.util.List;

@RestController
@Slf4j
public class PaymentController {

    @Autowired
    PaymentMethodService paymentMethodService;

    @PostMapping("/payment")
    public List <ResponseEntity<String>> sendPayment(@RequestBody String paymentInfo) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        log.info(paymentInfo);

        PaymentMethod paymentTemp;

        try {
            paymentTemp = objectMapper.readValue(paymentInfo, PaymentMethod.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        List <ResponseEntity<String>> responseEntities = new ArrayList<>();
        responseEntities.add(new ResponseEntity<String>("The Payment went through successfully", HttpStatus.OK));
        responseEntities.add(paymentMethodService.sendTransaction(paymentTemp));

        return responseEntities;

    }

}
