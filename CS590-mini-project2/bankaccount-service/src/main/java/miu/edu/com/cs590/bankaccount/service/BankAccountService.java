package miu.edu.com.cs590.bankaccount.service;

import miu.edu.com.cs590.bankaccount.model.BankAccount;
import org.springframework.stereotype.Service;

import java.util.List;

public interface BankAccountService {
    List<BankAccount> getAccounts();
    BankAccount saveBankAccount(BankAccount bankAccount);
    BankAccount getByName(String name);
    BankAccount getByRouting(String routing);

    BankAccount getById(Integer id);
}
