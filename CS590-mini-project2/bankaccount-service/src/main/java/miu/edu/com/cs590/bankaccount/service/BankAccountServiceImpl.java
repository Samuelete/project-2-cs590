package miu.edu.com.cs590.bankaccount.service;

import miu.edu.com.cs590.bankaccount.model.BankAccount;
import miu.edu.com.cs590.bankaccount.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Override
    public List<BankAccount> getAccounts() {
        return bankAccountRepository.findAll();
    }

    @Override
    public BankAccount saveBankAccount(BankAccount bankAccount) {
        return bankAccountRepository.save(bankAccount);
    }

    @Override
    public BankAccount getByName(String name) {
        return bankAccountRepository.getByName(name);
    }

    @Override
    public BankAccount getByRouting(String routing) {
        return bankAccountRepository.getByRouting(routing);
    }

    @Override
    public BankAccount getById(Integer bankId) {
        return bankAccountRepository.findById(bankId).get();
    }
}
