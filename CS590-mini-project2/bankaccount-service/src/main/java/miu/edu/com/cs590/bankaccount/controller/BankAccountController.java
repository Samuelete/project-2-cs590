package miu.edu.com.cs590.bankaccount.controller;

import miu.edu.com.cs590.bankaccount.model.BankAccount;
import miu.edu.com.cs590.bankaccount.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BankAccountController {

    @Autowired
    BankAccountService bankAccountService;

    @GetMapping("/bank")
    public List<BankAccount> getAccounts() {
        return bankAccountService.getAccounts();
    }

    @GetMapping("/bank/{name}")
    public BankAccount getAccount(@PathVariable String name) {
        return bankAccountService.getByName(name);
    }

    @GetMapping("/bank-id/{bankId}")
    public BankAccount getAccountById(@PathVariable Integer bankId) {
        return bankAccountService.getById(bankId);
    }

    @PostMapping("/bank")
    public BankAccount createBankAccount(@RequestBody BankAccount bankAccount) {
        return bankAccountService.saveBankAccount(bankAccount);
    }
    @GetMapping("/bank-routing/{routing}")
    public BankAccount getAccountByRouting(@PathVariable String routing)  {
        return bankAccountService.getByRouting(routing);
    }



}
