package miu.edu.com.cs590.bankaccount.repository;

import miu.edu.com.cs590.bankaccount.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountRepository extends JpaRepository<BankAccount, Integer> {
    BankAccount getByName(String name);
    BankAccount getByRouting(String routing);
}
