package com.miu.edu.cs590.paypal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TransactionPaypalServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionPaypalServiceApplication.class, args);
    }

}
