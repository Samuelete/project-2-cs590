package com.miu.edu.cs590.paypal.service;

import com.miu.edu.cs590.paypal.model.PaymentMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface TransactionService {

    ResponseEntity<String> transactionReceived(PaymentMethod paymentTemp);
}
