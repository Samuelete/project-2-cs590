package com.miu.edu.cs590.applicationgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ApplicationGateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationGateWayApplication.class, args);
    }

}
