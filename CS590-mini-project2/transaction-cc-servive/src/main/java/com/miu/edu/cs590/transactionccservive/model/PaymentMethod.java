package com.miu.edu.cs590.transactionccservive.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class PaymentMethod {
    private String PaymentInfo;
    private String customerName;
}
