package com.miu.edu.cs590.transactionccservive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TransactionCcServiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionCcServiveApplication.class, args);
    }

}
