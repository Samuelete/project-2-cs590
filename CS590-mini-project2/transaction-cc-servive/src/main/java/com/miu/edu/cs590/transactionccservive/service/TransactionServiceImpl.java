package com.miu.edu.cs590.transactionccservive.service;

import com.miu.edu.cs590.transactionccservive.model.PaymentMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {

    @Override
    public ResponseEntity<String> transactionReceived(PaymentMethod paymentTemp) {
        log.info("Dear " + paymentTemp.getCustomerName() + " The Credit Card Transaction have been made successfully");
        return new ResponseEntity<String>("Mr/Miss " + paymentTemp.getCustomerName() + " The Credit Card Transaction have been made successfully", HttpStatus.OK);
    }
}
