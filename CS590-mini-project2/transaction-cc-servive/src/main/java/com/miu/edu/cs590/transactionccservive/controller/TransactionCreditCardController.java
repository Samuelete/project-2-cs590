package com.miu.edu.cs590.transactionccservive.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.miu.edu.cs590.transactionccservive.model.PaymentMethod;
import com.miu.edu.cs590.transactionccservive.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TransactionCreditCardController {

    @Autowired
    TransactionService transactionService;

    @PostMapping("/transaction")
    public ResponseEntity<String> transactionReceived(@RequestBody String paymentInfo) {

        ObjectMapper objectMapper = new ObjectMapper();
        log.info(paymentInfo);

        PaymentMethod paymentTemp;

        try {
            paymentTemp = objectMapper.readValue(paymentInfo, PaymentMethod.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        return transactionService.transactionReceived(paymentTemp);
    }

}
