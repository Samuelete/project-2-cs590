package com.miu.edu.cs590.transactionccservive.service;

import com.miu.edu.cs590.transactionccservive.model.PaymentMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface TransactionService {

    ResponseEntity<String> transactionReceived(PaymentMethod paymentTemp);

}
